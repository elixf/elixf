import React from 'react';

import {ArgonCover, Layout} from '../../components';

class Dashboard extends React.Component {
  render() {
    return (
      <Layout id="dashboard">
        <ArgonCover/>
      </Layout>
    );
  }
}

export default Dashboard;
