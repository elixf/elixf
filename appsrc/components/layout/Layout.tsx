import React from 'react';

import '../../styles/appsrc.scss';

declare interface IProps {
  id?: string;
}

class Layout extends React.Component<IProps> {
  public render() {
    const {children, ...moreProps} = this.props;

    return (
      <section {...moreProps}>
        {children}
      </section>
    );
  }
}

export default Layout;
