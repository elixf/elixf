import React from 'react';

interface IProps {
}

interface IState {
}

class ArgonCover extends React.Component<IProps, IState> {
  render() {
    return (
      <section className="section-profile-cover section-shaped my-0">
        <div className="shape shape-style-1 shape-default alpha-4">
          <span/> <span/> <span/> <span/> <span/> <span/> <span/>
        </div>

        <div className="separator separator-bottom separator-skew">
          <svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" version="1.1" viewBox="0 0 3560 100">
            <polygon className="fill-white" points="3560 0, 3560 100, 0 100"/>
          </svg>
        </div>
      </section>
    );
  }
}

export default ArgonCover;
